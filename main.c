#include <errno.h>
#include <fcntl.h>
#include <liburing.h>
#include <netinet/in.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define O_DIRECT 0x40000
#define QD 1024
#define PORT 8080
#define HTTP_RESPONSE                                                          \
  "HTTP/1.1 200 OK\r\nServer: v.d/1.7\r\nDate: Mon, 24 Jul 2019 17:39:14 "     \
  "GMT\r\nContent-Type: text/plain\r\nContent-Length: 13\r\n\r\nHello, World!"

static void set_nonblocking(int fd) {
  int flags = fcntl(fd, F_GETFL, 0);
  if (flags == -1) {
    perror("fcntl()");
    return;
  }
  if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
    perror("fcntl()");
  }
}

int main() {
  struct io_uring ring;
  int i, fd, ret, pending, done;
  struct io_uring_sqe *sqe;
  struct io_uring_cqe *cqe;

  ret = io_uring_queue_init(QD, &ring, 0);
  if (ret < 0) {
    fprintf(stderr, "queue_init: %s\n", strerror(-ret));
    return 1;
  }

  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1) {
    perror("socket()");
    return 1;
  }

  int enable = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) ==
      -1) {
    perror("setsockopt()");
    return 1;
  }

  // bind
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  addr.sin_port = htons(PORT);
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("bind()");
    return 1;
  }

  // make it nonblocking, and then listen
  set_nonblocking(sock);
  if (listen(sock, SOMAXCONN) < 0) {
    perror("listen()");
    return 1;
  }
  sqe = io_uring_get_sqe(&ring);
  if (!sqe) {
    return 4;
  }
  io_uring_prep_poll_add(sqe, sock, POLLIN);
  sqe->user_data = sock;

  ret = io_uring_submit(&ring);
  if (ret < 0) {
    fprintf(stderr, "io_uring_submit: %s\n", strerror(-ret));
    return 1;
  }
  while (1) {
    io_uring_wait_cqe(&ring, &cqe);
    if (cqe == NULL) {
      return 3;
    }
    int need_resubmit = 0;
    short resubmit_mask = 0;
    unsigned cqe_flags = cqe->flags;
    unsigned long long cqe_user_data = cqe->user_data;
    int cqe_res = cqe->res;
    io_uring_cqe_seen(&ring, cqe);

    // printf("HERE 1 %u, sock: %u", cqe_user_data, sock);
    // accepting
    if (cqe_user_data == sock) {
      need_resubmit = sock;
      resubmit_mask = POLLIN;
      for (;;) {
        struct sockaddr in_addr;
        socklen_t in_addr_len = sizeof(in_addr);
        int client = accept(sock, &in_addr, &in_addr_len);
        if (client == -1) {
          if (errno == EAGAIN || errno == EWOULDBLOCK) {
            // we processed all of the connections
            break;
          } else {
            perror("accept()");
            return 1;
          }
        } else {
          printf("accept client");
          set_nonblocking(client);
          sqe = io_uring_get_sqe(&ring);
          if (!sqe) {
            return 4;
          }
          io_uring_prep_poll_add(sqe, client, POLLIN);
          sqe->user_data = client;
        }
      }
    } else {
      int client = cqe_user_data;
      resubmit_mask = POLLIN;
      if (cqe_res & POLLIN) {
        // resubmit_mask |= PollMask.pollin;
        char buf2[1024];
        for (;;) {
          ssize_t nbytes = read(client, buf2, 1024);
          if (nbytes == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
              need_resubmit = client;
              break;
            } else {
              close(client);
            }
          } else if (nbytes == 0) {
            break;
          } else {
            need_resubmit = client;
            resubmit_mask |= POLLOUT;
          }
        }
      }
      if (cqe_res & POLLOUT) {
        // resubmit_mask |= PollMask.pollout;
        ssize_t nbytes = write(client, HTTP_RESPONSE, strlen(HTTP_RESPONSE));
        if (nbytes == -1) {
          if (errno == EAGAIN || errno == EWOULDBLOCK) {
            need_resubmit = client;
          } else {
            perror("read()");
            return 1;
          }
        } else if (nbytes == 0) {
          close(client);
          need_resubmit = 0;
        } else {
          need_resubmit = client;
        }
      }
    }

    if (need_resubmit) {
      sqe = io_uring_get_sqe(&ring);
      if (!sqe) {
        return 4;
      }
      io_uring_prep_poll_add(sqe, need_resubmit, resubmit_mask);
      sqe->user_data = need_resubmit;
      ret = io_uring_submit(&ring);
      if (ret < 0) {
        fprintf(stderr, "io_uring_submit: %s\n", strerror(-ret));
        return 1;
      }
    }
  }

  close(sock);
  io_uring_queue_exit(&ring);

  return 0;
}